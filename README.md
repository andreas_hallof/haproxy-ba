# haproxy-ba #

PoC: HTTP Basic Auth mit HAProxy 

Es werden zwei minimalen Test-HTTP-Webserver als Test gestartet (localhost,
jeweils port 9001 und 9002). Sie besitzen keine Rechterverwaltung (also
insbesondere keine BasicAuth-Unterstützung). Als Frontend sitzt ein HAProxy,
der 

1. alle HTTP-Request auf / auf den Webserver auf 90001 weiterleitet
2. alle HTTP-Request auf /conan/ auf den Webserver auf 90002 weiterleitet
3. die BA je nach Webserver unterschiedlich durchführt (unterschiedliche Nutzergruppe).

## minimale Test-HTTP-Webserver
Die minimalen Test-HTTP-Webserver sind in wenigen Zeilen python geschrieben.
Ggf. "pip3 install flask" noch ausführen oder mit den apt-get/emerge/pacman/ ... 
installieren.

Die Webserver laufen auf localhost, könnten aber quasi beliebige IP-Adressen (VM1, VM2) haben.

	$ ./ws-1.py
	 * Running on http://127.0.0.1:9001/ (Press CTRL+C to quit)
	 * Restarting with stat
	 * Debugger is active!
	 * Debugger PIN: 188-639-583

In einem anderen Term

	$ ./ws-2.py
	 * Running on http://127.0.0.1:9002/ (Press CTRL+C to quit)
	 * Restarting with stat
	 * Debugger is active!
	 * Debugger PIN: 188-639-583

Testen:

	$ curl http://127.0.0.1:9001/ -o -
	<!DOCTYPE html><html lang="de"><head><meta charset="utf-8">
	<title> WS1 </title> </head> <body>Hallo Welt, von WS 1</body></html>

	$ curl http://localhost:9002/conan/ -o -
	<!DOCTYPE html><html lang="de"><head><meta charset="utf-8">
	<title> WS2 </title> </head> <body>Hallo Welt, von WS 2 /conan/ </body></html>


Wie man sieht gibt es keine Rechtebeschränkung.

## haproxy erst einmal ohne BA ##

haproxy starten

	haproxy -f haproxy.cfg

Test

	$ curl http://127.0.0.1:9000/ -o -
	<!DOCTYPE html><html lang="de"><head><meta charset="utf-8">
	<title> WS1 </title> </head> <body>Hallo Welt, von WS 1</body></html>

	$ curl http://127.0.0.1:9000/conan/ -o -
	<!DOCTYPE html><html lang="de"><head><meta charset="utf-8">
	<title> WS2 </title> </head> <body>Hallo Welt, von WS 2 /conan/ </body></html>

	$ curl http://127.0.0.1:9000/conan/dkjefhewhfdfdw -o -
	<!DOCTYPE html><html lang="de"><head><meta charset="utf-8">
	<title> WS2 </title> </head> <body>Hallo Welt, von WS 2 /conan/dkjefhewhfdfdw </body></html>


	$ cat haproxy.cfg
	frontend http-in
	    bind 127.0.0.1:9000
	    acl has_conan_uri path_beg /conan
	    use_backend conan_server if has_conan_uri
	    default_backend cockpit

	backend cockpit
	    mode http
	    option httpclose
	    server webA 127.0.0.1:9001 

	backend conan_server
	    mode http
	    option httpclose
	    server webC 127.0.0.1:9002

## haproxy mit BA ##

haproxy mit neuer Konfiguration starten

        haproxy -f haproxy-ba.cfg

Test (kein Username:Password)

        $ curl http://127.0.0.1:9000/conan/ -o -
        <html><body><h1>403 Forbidden</h1>
        Request forbidden by administrative rules.
        </body></html>

Test (falsches Passwort)

        $ curl -u tom:tom http://127.0.0.1:9000/
        <html><body><h1>403 Forbidden</h1>
        Request forbidden by administrative rules.
        </body></html>

Test (Username und Password sind richtig)

        $ curl -u tom:abcd http://127.0.0.1:9000/
        <!DOCTYPE html><html lang="de"><head><meta charset="utf-8">
        <title> WS1 </title> </head> <body>Hallo Welt, von WS 1</body></html>

Test (Nutzer tom kann nicht auf /conan zugreifen)

        $ curl -u tom:abcd http://127.0.0.1:9000/conan/
        <html><body><h1>403 Forbidden</h1>
        Request forbidden by administrative rules.
        </body></html>

Test (Nutzer chris darf auf / und auch auch /conan zureifen)

    $ curl -u chris:abcd http://127.0.0.1:9000/
    <!DOCTYPE html><html lang="de"><head><meta charset="utf-8">
    <title> WS1 </title> </head> <body>Hallo Welt, von WS 1</body></html>

    $ curl -u chris:abcd http://127.0.0.1:9000/conan/
    <!DOCTYPE html><html lang="de"><head><meta charset="utf-8">
    <title> WS2 </title> </head> <body>Hallo Welt, von WS 2 /conan/ </body></html>

Konfiguration


    $ cat haproxy-ba.cfg
    # https://www.haproxy.org/download/1.8/doc/configuration.txt
    
    frontend http-in
        bind 127.0.0.1:9000
        acl has_conan_uri path_beg /conan
        use_backend conan if has_conan_uri
        default_backend cockpit
    
    
    userlist UsersFor_cockpit
        group g1
    
        user chris insecure-password abcd groups g1
        user tom insecure-password abcd groups g1
    
    backend cockpit
        mode http
        option httpclose
        server webA 127.0.0.1:9001 
    
        acl auth_cockpit http_auth(UsersFor_cockpit)
    
        http-request auth realm cockpit unless auth_cockpit
        http-request allow if auth_cockpit
    
        http-request deny
    
    
    userlist UsersFor_conan
        group g2
    
        user chris insecure-password abcd groups g2
    
    backend conan
        mode http
        option httpclose
        server webC 127.0.0.1:9002
    
        acl auth_conan http_auth(UsersFor_conan)
    
        http-request auth realm conan unless auth_conan
        http-request allow if auth_conan
    
        http-request deny
    
