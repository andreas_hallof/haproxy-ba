#! /usr/bin/env python3

from flask import Flask

app = Flask(__name__)

HTML_PREFIX='''<!DOCTYPE html><html lang="de"><head><meta charset="utf-8">
<title> WS2 </title> </head> <body>'''

HTML_SUFFIX='</body></html>\n'

@app.route("/")
def hello():
    return HTML_PREFIX+"Hallo Welt, von WS 2"+HTML_SUFFIX

@app.route("/conan")
def conan():
    return HTML_PREFIX + "Hallo Welt, von WS 2 /conan/ " + HTML_SUFFIX

@app.route("/conan/")
def conan_p():
    return HTML_PREFIX + "Hallo Welt, von WS 2 /conan/ " + HTML_SUFFIX

@app.route("/conan/<test>")
def conan_plus(test):
    return HTML_PREFIX + "Hallo Welt, von WS 2 /conan/" + test + " " + HTML_SUFFIX

if __name__ == '__main__':

    app.run( port=9002, debug=True )

