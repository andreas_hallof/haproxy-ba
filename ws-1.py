#! /usr/bin/env python3

from flask import Flask

app = Flask(__name__)

HTML_PREFIX='''<!DOCTYPE html><html lang="de"><head><meta charset="utf-8">
<title> WS1 </title> </head> <body>'''

HTML_SUFFIX='</body></html>\n'

@app.route("/")
def hello():
    return HTML_PREFIX+"Hallo Welt, von WS 1"+HTML_SUFFIX

if __name__ == '__main__':

    app.run( port=9001, debug=True )

